.data
 /* Definicion de datos */
mapa: .asciz "+------------------------------------------------+\n|               ****************                 |\n|               *** VIBORITA ***                 |\n|               ****************                 |\n+------------------------------------------------+\n|                                                |\n|                                                |\n|         @***                                   |\n|                                                |\n|                    M                           |\n|                                                |\n|                                                |\n|                                                |\n|                                                |\n+------------------------------------------------+\n| Puntaje:                      Nivel:           |\n+------------------------------------------------+\n" 
longitud = . - mapa

cls: .asciz "\x1b[H\x1b[2J" @ una manera de borrar la pantalla usando ansi escape codes
lencls = .-cls                @ Borra la pantalla :)

.text 			@ Definición de código del programa
.global main		@ global, visible en todo el programa
main:
	mov r7, #4	@ Salida por pantalla  
	mov r0, #1      @ Indicamos a SWI que será una cadena
	
    ldr r2, =longitud @Tamaño de la cadena
	ldr r1, =mapa@ Cargamos en r1 la dirección del mensaje
 	swi 0		@ SWI, Software interrupt
	
	mov r0, #1
	ldr r1, =cls
	ldr r2, =lencls
	mov r7, #4
	swi #0
	
	ldr r1, =mapa@ Cargamos en r1 la dirección del mensaje
	mov r5,#'*'
	strb r5,[r1,#683]
	
	mov r7, #4	@ Salida por pantalla  
	mov r0, #1      @ Indicamos a SWI que será una cadena
	
    ldr r2, =longitud @Tamaño de la cadena
	ldr r1, =mapa@ Cargamos en r1 la dirección del mensaje
 	swi 0		@ SWI, Software interrupt
	
	mov r7, #1	@ Salida al sistema
	swi 0