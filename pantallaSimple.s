.data
 /* Definicion de datos */
mapa: .asciz "+------------------------------------------------+\n|               ****************                 |\n|               *** VIBORITA ***                 |\n|               ****************                 |\n+------------------------------------------------+\n|                                                |\n|                                                |\n|         @***                                   |\n|                                                |\n|                    M                           |\n|                                                |\n|                                                |\n|                                                |\n|                                                |\n+------------------------------------------------+\n| Puntaje:                      Nivel:           |\n+------------------------------------------------+\n" 
snake: .asciz "@******"
posicionSnake: .int 601
longitud = . - mapa


.text 			@ Definición de código del programa
.global main		@ global, visible en todo el programa
main:
    
    ldr r1, =mapa@ Cargamos en r1 la dirección del mensaje
    ldr r0,=posicionSnake
    ldr r4,[r0]
    ldr r0,=snake
    mov r2,#0
    ldrb r3,[r0]
    strb r3,[r1,r4]
com:                     
     
    cmp r3,#0
    beq continuar

    add r2,#1
    add r4,#1  
    ldrb r3,[r0,r2]
    strb r3,[r1,r4]
    bal com


continuar:
	mov r7, #4	@ Salida por pantalla  
	mov r0, #1      @ Indicamos a SWI que será una cadena
	
    ldr r2, =longitud @Tamaño de la cadena
	ldr r1, =mapa@ Cargamos en r1 la dirección del mensaje
 	swi 0		@ SWI, Software interrupt

	mov r7, #1	@ Salida al sistema
	swi 0     