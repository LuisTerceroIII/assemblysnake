.data
 /* Definicion de datos */
mapa: .asciz "+------------------------------------------------+\n|               ****************                 |\n|               *** VIBORITA ***                 |\n|               ****************                 |\n+------------------------------------------------+\n|                                                |\n|                                                |\n|         @***                                   |\n|                                                |\n|                    M                           |\n|                                                |\n|                                                |\n|                                                |\n|                                                |\n+------------------------------------------------+\n| Puntaje:                      Nivel:           |\n+------------------------------------------------+\n" 
longitud = . - mapa

cls: .asciz "\x1b[H\x1b[2J" @ una manera de borrar la pantalla usando ansi escape codes
lencls = .-cls                @ Borra la pantalla :)

gameover: .byte 0 @ 0 = falso , 1 = verdadero

mensajeMovimiento: .asciz "Ingrese instruccion de movimiento:     "
tecla: .byte ' '

numero: .int 618


.text @ Definición de código del programa

play: @ While principal
    .fnstart
        push {lr}
        cmp r12,#1 @ Si r12 = 1 -> Game over
        beq fin @ fin

        bal imprimirPantalla
        pop {lr}

        bx lr
    .fnend

imprimirPantalla: @ Se imprime la pantalla
    .fnstart
        push {lr}
        mov r7, #4	@ Salida por pantalla  
        mov r0, #1      @ Indicamos a SWI que será una cadena
        ldr r2, =longitud @Tamaño de la cadena
        ldr r1, =mapa@ Cargamos en r1 la dirección del mensaje
        swi 0		@ SWI, Software interrupt
        
        bal leerTecla

        pop {lr}
        bx lr    
    .fnend   

leerTecla: @ Procesar entrada
    .fnstart
        push {lr}

        mov r7, #4	@ Salida por pantalla  
        mov r0, #1      @ Indicamos a SWI que será una cadena
        ldr r1, =mensajeMovimiento@ Cargamos en r1 la dirección del mensaje
        swi 0		@ SWI, Software interrupt

        mov r7,#3 @Lectura teclado
        mov r0,#0 @Ingreso cadena
        mov r2,#1 @cantidad caracteres a escribir
        ldr r1,=tecla
        swi 0

        ldrb r5,[r1]

        cmp r5,#'d'
        beq derecha

        cmp r5,#'a'
        beq izquierda
        
        cmp r5,#'w'
        beq arriba

        cmp r5,#'s'
        beq abajo

        cmp r5,#'q'
        beq fin

    derecha:
        ldr r1, =mapa @ Cargamos en r1 la dirección del mensaje
        mov r4,#'*'
        add r8,#1
        strb r4,[r1,r8]  
        bal limpiar

    izquierda:
        ldr r1, =mapa @ Cargamos en r1 la dirección del mensaje
        mov r4,#'*'
        sub r8,#1
        strb r4,[r1,r8]  
        bal limpiar    

    arriba:
        ldr r1, =mapa @ Cargamos en r1 la dirección del mensaje
        mov r4,#'*'
        sub r8,#51
        strb r4,[r1,r8]   
        bal limpiar

    abajo:
        ldr r1, =mapa @ Cargamos en r1 la dirección del mensaje
        mov r4,#'*'
        add r8,#51
        strb r4,[r1,r8]  
        bal limpiar    


    
    limpiar:
    
        bal limpiarPantalla
        pop {lr}
        

       

 

        
        @ obtenerNuevaPosicion: Adonde tiene que avanzar la vibora (fila,columna)
        @ hayManzana: Verificar si en la nueva posicion hay una 'M'
        @ comerManzana: Si hay manzana, comer, incrementar largo y puntaje
        @ hayPared: Verificar si hay pared '-' o '|'
        @ imprimirGameOver: Imprimir mensaje gameover si hay pared
        @ avanzarANuevaPosicionViborita: actualizar cuerpo de vibora
        @ generarManzana: Generar una 'M' en un espacio valido
        @ actualizarPuntaje: actualizar puntaje, basado en la longitud de la vibora

        bx lr
    .fnend   
 

gameOver:
    .fnstart
        /*
            Detectar colisiones
                y si hay colision, actualizar registro 12 a 1
        */
        @ cmp snake,#'-'
        @ cmp snake,#'|'
        @ mov r12,#1
        push {lr}
        bal fin
        pop {lr}
        bx lr
    .fnend    

limpiarPantalla:
    .fnstart
       
        push {lr}
        mov r0, #1
        ldr r1, =cls
        ldr r2, =lencls
        mov r7, #4
        swi #0
        
        cmp r12,#1
        beq gameOver

        bal play

        bx lr
    .fnend    

/*
Game Over  -> 1 verdadero - 0 falso -> 1 termina el juego, gameover - 0 continua
r12 = gameover

*/
.global main

main:

    ldr r0,=gameover @ Cargamos la direccion de gameover en r0
    ldrb r12,[r0]    @ Cargamos el valor de gameover en r12

    ldr r1,=numero
    ldr r8,[r1]

juego:
    bal play


fin:
    mov r7,#1
    swi 0
